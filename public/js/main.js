// Mensajes Iniciales
alert(" ¡Bienvenidas mujeres emprendedoras! ")
if(window.confirm(" ¿Deseas saber acerca de autonomía económica de las mujeres? ")){
    nombreMujer = window.prompt("Ingresa tu Nombre: ");
    alert("Hola " +  nombreMujer +" te invitamos a ser emprendedora y a desarrollar tu autonomía económica.");
    window.open("../html/index.html");    
}

// Carrousel
window.addEventListener('load', function(){
    new Glider(document.querySelector('.carousel-lista'),{
        slidesToShow: 3,
        dots: '.carousel-indicadores',
        arrows: {
            prev: '.anterior-carousel',
            next: '.siguiente-carousel'
        }
    });
});